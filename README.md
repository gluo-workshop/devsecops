# Lab 00 - Gluo DevSecOps workshop

Welcome to the Gluo DevSecOps workshop! 

In this workshop you will discover more about DevSecOps. Throughout the labs, you will work with Terraform, Docker, Kubernetes and Helm, whilst discovering and fixing configuration- and security-related issues coming from Trivy, Gatekeeper and nikto. In the end, a simple website should be running on a Kubernetes cluster.

## &#x2757; **Important:** Before you start the workshop make sure that the steps in this chapter are completed!   

:bulb: Open a duplicate tab (and split your screen) so you have the instructions below opened at all times while you process each step. 

### :bust_in_silhouette: Make sure you have a Gitlab.com account (you can create one for free)
1. Provide your instructor with the numerical userID and email of your Gitlab account, so he can assign you to your team. You should have filled in this [Google Forms](https://forms.gle/7X7cCk96b7e3bHz9A) earlier.
2. This [public repo](https://gitlab.com/gluo-workshop/devsecops) is forked into your GitLab group under `https://gitlab.com/gluo-workshop/devsecops-workshop/teamX/devsecops`. You can find your group and repo by expanding the `devsecops-workshop` group at [`gitlab.com/gluo-workshop`](https://gitlab.com/gluo-workshop).

![Gitlab group](./images/gitlab-groups.png)

## :hammer_and_wrench: Prerequisites

### :gear: Create a Personal Access token in your Gitlab account

A Personal Access Token (PAT) will be used for authenticating your Gitlab account during API-calls and during the lab checks.  

1. Click your profile picture in the top right of the screen to open a drop-down list
2. Select **Preferences**
3. In the menu bar on the left, pick **Access Tokens**
4. Give the token a name (e.g. *gluo-workshop*)
5. Select scopes:  
\- *api*
6. Click the button **Create personal access token**  
\- The access token now appears (masked with \*\*\*\*\*), copy the value (you can click the copy icon) and save it somewhere, you will need it later on

### :gear: Create CI/CD variables in your workshop repository

The token created in the previous section will now be stored as a variable to be used in your project. 

1. Still in the repository, navigate to **Settings -> CI/CD**
2. Click **Expand** in the **Variables** section
3. Click **Add variable**, and a pop-up window opens. Fill out:  
   \- Key: **STUDENT_PERSONAL_ACCESS_TOKEN**  
   \- Value: access token from chapter 'Create a Personal Access token in your Gitlab account' step 6  
   \- Flags:  
    - Protect variable: *true*  
    - Mask variable: *true*  
    - Expand variable reference: *false*
4. Click **Add variable** to save

### :gear: Run the pipeline for the first time to check if your PAT is set correctly

1. Run the pipeline for lab 0 the first time
2. In the left menu, go to **CI/CD**, then **Pipelines** and click the **Run pipeline** button
3. In the next screen, set `input variable key` to **LAB** and `input variable value` to **0**
4. Click **Run pipeline**
5. The pipeline will now run, and you can follow the progress in the pipeline overview. Check the `getting_started` job, it should be green. If it is red, check the logs and make sure you have set the PAT correctly. You can click the job name to open the logs.	

:information_source: You will have to repeat this for every lab, for lab01 the `input variable value` will be **1**, and so on...

Example: 
![image-20230329115854938](./images/lab00_run_pipeline.png)

After you finished these prerequisites, open the repository in the folder [lab01](lab01/) to find the next instructions.
