# Lab 01 - Terraform

# :information_source: Summary

In this lab, you will be working with Terraform, a powerful tool that is used for infrastructure-as-code. After fixing errors concerning configuration and security, you will have created a new, personal nodepool on a Kubernetes cluster provided by your instructor. This node will be used to host your app at the end of the workshop. If you want to have a look around, you can access a Kubernetes dashboard at [`dashboard.azure.gluo.cloud/#/workloads?namespace=teamX`](https://dashboard.azure.gluo.cloud/#/workloads?namespace=teamX) (replace `X` with your team number). The access token will be provided at the end of the `terraform_apply` stage in the pipeline, save this somewhere. Because of the fact that you haven't deployed anything yet, you will see an empty dashboard. This will change after [lab04](/lab04)!

![dashboard](../images/lab01_dashboard.png)

# :hammer_and_wrench: Fix Terraform

## :gear: Attempt 1

1. Run the pipeline for lab 1 the first time

## :mag: Error 1 - Misconfiguration

![image-20230329125752187](../images/lab01_pipeline_error1.png)

```console
│ Error: Reference to undeclared resource
│ 
│   on output.tf line 2, in output "aks_fqdn":
│    2:   value = azurerm_kubernetes_cluster.aks.fqdn
│ 
│ A managed resource "azurerm_kubernetes_cluster" "aks" has not been declared
│ in the root module.
│ 
│ Did you mean the data resource data.azurerm_kubernetes_cluster.aks?
╵
```
1. Analyse the error message in the output of the failed stage in the pipeline
2. You will see that the pipeline fails at the *terraform_setup* stage. In this stage we use the `terraform validate` command to run a simple validation of the written terraform code
3. The stage fails, because something is wrong with the syntax of the code. Read the error carefully, to find out the filename and line-number where you should look to fix the issue
4. The error also suggests a possible solution, can you see what it is?
5. If you think you know the answer, edit the problematic file (you can do this directly in the browser) and commit the change

> :bulb: Background information: :link: [Terraform Docs - Data Sources](https://developer.hashicorp.com/terraform/language/data-sources)

:sos: If you're stuck or you have any problems, ask your instructor for a hint! They're here to help! :raising_hand:

## :gear: Attempt 2

1. Run the pipeline again

## :mag: Error 2 - Insecure configuration

```console
Violation Details -
    
	Description    :	Anonymous, public read access to a container and its blobs can be enabled in Azure Blob storage. This is only recommended if absolutely necessary.
	File           :	main.tf
	Module Name    :	root
	Plan Root      :	./
	Line           :	61
	Severity       :	HIGH
	
	-----------------------------------------------------------------------
	
Scan Summary -
	File/Folder         :	/builds/gluo-workshop/devsecops-workshop/team1/devsecops/lab01/terraform
	IaC Type            :	terraform
	Scanned At          :	2023-04-18 09:02:05.368104712 +0000 UTC
	Policies Validated  :	36
	Violated Policies   :	1
	Low                 :	0
	Medium              :	0
	High                :	1
```

1. Analyse the error message in the output of the failed stage in the pipeline
2. You will see that the pipeline now fails at the *terraform_scan* stage. Here we run a program called *terrascan* which scans the terraform code and detects compliance and security issues
3. This time the error does not suggest a solution. It does say (in a cryptic way) that *public access* to Azure storage containers is insecure
4. Take note of the filename and line-number, to quickly locate the problematic code, and take a look at the snippet:

```terraform
resource "azurerm_storage_container" "container" {
  name                  = lower("${random_string.random.result}kubeconfig")
  storage_account_name  = data.azurerm_storage_account.storage-account.name
  container_access_type = "blob"
}
```

5. You can see there is an option `container_access_type` with the value `blob`, whatever that means.
6. Anytime you do not know something, there's this thing called "internet". It does the remembering for you :grin:. Since we want to know what this bit of Terraform code means, we can look that up in the Terraform documentation
7. Look for possible options that you can use for `container_access_type` and if you think you know the solution, edit the file and commit the change

> :bulb: Background information: :link: [Terraform Docs - azurerm_storage_container](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/storage_container#argument-reference)

## :gear: Final attempt 

1. Run the pipeline again
2. The pipeline should now run successfully! :sunglasses:
3. Look at the scoreboard to see if your team has completed this lab! If your score is not updated, check the output of the *check_lab* stage and try again
4. Continue with [lab02](/lab02)
