az login --service-principal -u ${ARM_CLIENT_ID} -p ${ARM_CLIENT_SECRET} --tenant ${ARM_TENANT_ID} --only-show-errors -o none 
az account set --subscription ${ARM_SUBSCRIPTION_ID} --only-show-errors -o none
az aks get-credentials --resource-group ${RESOURCE_GROUP} --name ${CLUSTER} --only-show-errors -o none --overwrite-existing

CA=$(kubectl  -n ${NAMESPACE} get secret `kubectl -n ${NAMESPACE} get secret | (grep ${USER} || echo "$_") | awk '{print $1}'` -o "jsonpath={.data['ca\.crt']}")
TOKEN=$(kubectl -n ${NAMESPACE} describe secret $(kubectl -n ${NAMESPACE} get secret | (grep ${USER} || echo "$_") | awk '{print $1}') | grep token: | awk '{print $2}'\n)

rm -rf ./configs
mkdir -p ./configs

echo "
apiVersion: v1
kind: Config

clusters:
- cluster:
    certificate-authority-data: ${CA}
    server: https://${SERVER}:443
  name: ${CLUSTER}

contexts:
- context:
    cluster: ${CLUSTER}
    user: ${USER}
  name: ${USER}

current-context: ${USER}
preferences: {}

users:
- name: ${USER}
  user:
    token: ${TOKEN}
    client-key-data: ${CA}

" > ./configs/${FILE_NAME}

az storage blob upload --account-name ${STORAGE_ACCOUNT} --container-name ${CONTAINER_NAME} --file ./configs/${FILE_NAME} --overwrite --only-show-errors -o none
end=$(date -d@"$(( `busybox date +%s`+30*60 ))" '+%Y-%m-%dT%H:%MZ') # The token will be valid for 30 minutes

echo "_____________________________________________________________________________________"
echo "DOWNLOAD your kubeconfig via this link:"
echo ""
az storage blob generate-sas --account-name ${STORAGE_ACCOUNT} --container-name ${CONTAINER_NAME} -n ${FILE_NAME} --permissions r --expiry $end --https-only --full-uri --only-show-errors
echo ""
echo "YOU HAVE 30 MINUTES TO DOWNLOAD THE KUBECONFIG BEFORE THE LINK EXPIRES!!"
echo "_____________________________________________________________________________________"
