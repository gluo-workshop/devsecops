locals {
  # GENERAL
  prefix                = "gluo-devsecops-wks"
  location              = "northeurope"
  node_pool_name        = "${var.GROUP_NAME}"
  resource_group_name   = "${local.prefix}-rg"
  cluster_name          = "${local.prefix}-aks"
  storage_account_name  = replace("${local.prefix}st", "-", "")
}

data "azurerm_storage_account" "storage-account" {
  name                = local.storage_account_name
  resource_group_name = local.resource_group_name
}

data "azurerm_kubernetes_cluster" "aks" {
  name                = local.cluster_name
  resource_group_name = local.resource_group_name
}

module "aks-nodepool" {
    source = "gitlab.com/gluo-workshop/aks-nodepool/azure"
    version = "0.3.1"

    # GENERAL
    resource_group_name = local.resource_group_name
    location = local.location

    # NETWORK
    vnet_name = "gluo-devsecops-wks-vnet"
    aks_subnet_address_name = "gluo-devsecops-wks-aks-subnet"

    # AKS
    cluster_name = local.cluster_name
    ## Node pool ##
    node_pool_name = local.node_pool_name
    vm_size        = "standard_b2s"
    node_count     = 1
    ## EXTRAS ##
    create_nodepool_linked_namespace_and_service_account = true
    namespace_name = local.node_pool_name

    # TAGS
    tags = {
        provisioner = "terraform"
        project     = "devsecops"
        environment = "workshop"
    }
}

resource "random_string" "random" {
  length           = 20
  special          = false
  min_numeric = 5
  min_lower = 5
  min_upper = 5
}

// View more options: https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/storage_container
resource "azurerm_storage_container" "container" {
  name                  = lower("${random_string.random.result}kubeconfig")
  storage_account_name  = data.azurerm_storage_account.storage-account.name
  container_access_type = "blob"
}

resource "null_resource" "generate-kubeconfig" {
  triggers = {
    always_run = "${timestamp()}"
  }
  provisioner "local-exec" {
    command = "../scripts/generateKubeConfigs.sh"
    environment = {
      SERVER    = data.azurerm_kubernetes_cluster.aks.fqdn
      CLUSTER   = data.azurerm_kubernetes_cluster.aks.name
      USER      = "${local.node_pool_name}-serviceaccount"
      NAMESPACE = local.node_pool_name
      RESOURCE_GROUP = data.azurerm_kubernetes_cluster.aks.resource_group_name
      STORAGE_ACCOUNT = data.azurerm_storage_account.storage-account.name
      CONTAINER_NAME = azurerm_storage_container.container.name
      FILE_NAME = local.node_pool_name
    }
  }
  depends_on = [
    data.azurerm_kubernetes_cluster.aks,
    module.aks-nodepool
  ]
}
