output "aks_fqdn" {
  value = azurerm_kubernetes_cluster.aks.fqdn
  sensitive = true
}