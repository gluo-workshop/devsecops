# Lab 02 - Docker

# :information_source: Summary

In the previous lab, you provisioned a personal nodepool on the workshop's AKS cluster (Kubernetes on Azure). In this lab, you will be working with Docker. This lab will have some security-related issues in a Dockerfile for you to fix. At the end of this lab, you will have scanned, built and pushed a container image to this repo's Container Registry. This can be found in the left menu -> **Packages and registries -> Container Registry**.

![ctr-registry](../images/lab02_container_registry.png)

# :hammer_and_wrench: Fix Dockerfile

## :gear: Attempt 1

1. Run the pipeline for lab 2 for the first time

## :mag: Error 1 - Insecure Dockerfile

![image-20221222141557923](../images/lab02_pipeline_error.png)

```console
Dockerfile (dockerfile)
=======================
Tests: 23 (SUCCESSES: 22, FAILURES: 1, EXCEPTIONS: 0)
Failures: 1 (LOW: 1, MEDIUM: 0, HIGH: 0, CRITICAL: 0)
LOW: Consider using 'COPY ./app /usr/share/nginx/html' command instead of 'ADD ./app /usr/share/nginx/html'
════════════════════════════════════════
You should use COPY instead of ADD unless you want to extract a tar file. Note that an ADD command will extract a tar file, which adds the risk of Zip-based vulnerabilities. Accordingly, it is advised to use a COPY command, which does not extract tar files.
See https://avd.aquasec.com/misconfig/ds005
────────────────────────────────────────
 Dockerfile:5
────────────────────────────────────────
   5 [ ADD ./app /usr/share/nginx/html
────────────────────────────────────────
```

1. Analyse the error message in the output of the failed stage in the pipeline
2. The pipeline fails in the *trivy_config_scan* stage. In this stage, the Dockerfile is scanned for security issues using Trivy
3. This stage will fail because Trivy has found an issue with the Dockerfile
4. Trivy proposes a solution to the issue, can you spot it? 
5. Fix the [`Dockerfile`](/lab02/Dockerfile) and push the edited code to your Gitlab repository

> :bulb: Background information: :link: [Dockerfile reference](https://docs.docker.com/engine/reference/builder/#add)

## :gear: Attempt 2

1. Run the pipeline again

## :mag: Error 2 - Vulnerable packages in image

```console
Total: 51 (LOW: 4, MEDIUM: 12, HIGH: 29, CRITICAL: 6)
┌──────────────┬────────────────┬──────────┬───────────────────┬───────────────┬──────────────────────────────────────────────────────────────┐
│   Library    │ Vulnerability  │ Severity │ Installed Version │ Fixed Version │                            Title                             │
├──────────────┼────────────────┼──────────┼───────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ apk-tools    │ CVE-2021-36159 │ CRITICAL │ 2.10.6-r0         │ 2.10.7-r0     │ libfetch before 2021-07-26, as used in apk-tools, xbps, and  │
│              │                │          │                   │               │ other products, mishandles...                                │
│              │                │          │                   │               │ https://avd.aquasec.com/nvd/cve-2021-36159                   │
├──────────────┼────────────────┼──────────┼───────────────────┼───────────────┼──────────────────────────────────────────────────────────────┤
│ busybox      │ CVE-2021-42378 │ HIGH     │ 1.31.1-r10        │ 1.31.1-r11    │ busybox: use-after-free in awk applet leads to denial of     │
│              │                │          │                   │               │ service and possibly...                                      │
│              │                │          │                   │               │ https://avd.aquasec.com/nvd/cve-2021-42378                   │
│              ├────────────────┤          │                   │               ├──────────────────────────────────────────────────────────────┤

... (truncated example output)
```

1. Analyse the error message in the output of the failed stage in the pipeline
2. Running the pipeline will show how the Trivy scan in stage 1 succeeds and continues to stage 2, where the the Docker image will be built
3. The *trivy_dependency_scan* stage will ultimately fail because Trivy will scan the Docker image and issues regarding some packages will be shown in the output (these can change over time)
4. Fix the [`Dockerfile`](/lab02/Dockerfile) vulnerability and push the edited code to your Gitlab repository

> :bulb: Background information: :link: [Official nginx images on Docker Hub](https://hub.docker.com/_/nginx)

## :gear: Final attempt 

1. Run the pipeline again
2. The pipeline should now run successfully! :sunglasses:
3. Look at the scoreboard to see if your team has completed this lab! If your score is not updated, check the output of the *check_lab* stage and try again
4. Continue with [lab03](/lab03)