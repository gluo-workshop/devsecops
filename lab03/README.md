# Lab 03 - Helm

# :information_source: Summary

In the previous lab, you pushed a Docker image to your repo's Container Registry. In this lab, that image wil be used to create a Helm Chart. This Helm Chart will be scanned for security issues using Trivy. When the scan succeeds, the Chart will be packaged and stored in your repo's Package Registry. This can be found in the left menu -> **Packages and registries -> Package Registry**.

![pkg-registry](../images/lab03_package_registry.png)

# :hammer_and_wrench: Fix Helm template

## :gear: Attempt 1

1. Run the pipeline for lab 3 for the first time

## :mag: Error 1 - Insecure Helm template

![lab3-pipeline-error](../images/lab03_pipeline_error.png)

```console
demo_app/templates/deployment.yaml (kubernetes)
===============================================
Tests: 81 (SUCCESSES: 79, FAILURES: 2, EXCEPTIONS: 0)
Failures: 2 (MEDIUM: 1, HIGH: 1, CRITICAL: 0)
HIGH: Container 'demo-app' of Deployment 'demo-app' should not include 'SYS_ADMIN' in 'securityContext.capabilities.add'
════════════════════════════════════════
SYS_ADMIN gives the processes running inside the container privileges that are equivalent to root.
See https://avd.aquasec.com/misconfig/ksv005
────────────────────────────────────────
 demo_app/templates/deployment.yaml:22-30
────────────────────────────────────────
  22 ┌         - name: demo-app
  23 │           image: ":"
  24 │           # Link: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/
  25 │           securityContext:
  26 │             capabilities:
  27 │               add: ["SYS_ADMIN"]
  28 │             allowPrivilegeEscalation: false
  29 │             runAsUser: 0
  30 └             runAsGroup: 0
────────────────────────────────────────
MEDIUM: Container 'demo-app' of Deployment 'demo-app' should not set 'securityContext.capabilities.add'
════════════════════════════════════════
Adding NET_RAW or capabilities beyond the default set must be disallowed.
See https://avd.aquasec.com/misconfig/ksv022
────────────────────────────────────────
 demo_app/templates/deployment.yaml:22-30
────────────────────────────────────────
  22 ┌         - name: demo-app
  23 │           image: ":"
  24 │           # Link: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/
  25 │           securityContext:
  26 │             capabilities:
  27 │               add: ["SYS_ADMIN"]
  28 │             allowPrivilegeEscalation: false
  29 │             runAsUser: 0
  30 └             runAsGroup: 0
────────────────────────────────────────
```

1. Analyse the error message in the output of the failed stage in the pipeline
2. The pipeline fails in the *trivy_scan* stage. In this stage, the Helm template is scanned for security issues using Trivy
3. This stage will fail because Trivy has found an issue with the Helm template
4. Fix the appropriate Helm Chart manifest file and push the edited code to your Gitlab repository

> :bulb: Background information: :link: [Kubernetes Security Context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/#set-capabilities-for-a-container)

## :gear: Final attempt 

1. Run the pipeline again
2. The pipeline should now run successfully! :sunglasses:
3. Look at the scoreboard to see if your team has completed this lab! If your score is not updated, check the output of the *check_lab* stage and try again
4. Continue with [lab04](/lab04)