# Lab 04 - Gatekeeper

# :information_source: Summary

In the previous lab, you stored a Helm Chart to your repo's Package Registry. In this lab, that Chart will be deployed to your own nodepool from Lab 1 on the AKS cluster. This Chart will only be deployed if it satisfies the security requirements of the cluster. In the end, you will have a running application in your cluster, which you can find at [`teamX.azure.gluo.cloud`](https://teamX.azure.gluo.cloud) (where 'X' is your team's number).

![webapp](../images/lab04_website.png)

# :hammer_and_wrench: Fix Kubernetes security constraints

## :gear: Attempt 1

1. Run the pipeline for lab 4 for the first time

## :mag: Error 1 - Denied Helm Chart deployment

![lab4-pipeline-error](../images/lab04_pipeline_error.png)

```console
error: timed out waiting for the condition on deployments/demo-app
LAST_SEEN              EVENT
2023-05-22T13:03:57Z   Error creating: admission webhook "validation.gatekeeper.sh" denied the request: [psp-pods-allowed-user-ranges] container [demo-app] has an invalid GID [0], disallowed GID is [0]
[psp-pods-allowed-user-ranges] container [demo-app] has an invalid UID [0], disallowed UID is [0]
```

1. Analyse the error message in the output of the failed stage in the pipeline
2. The pipeline fails in the *install_helm_chart_in_AKS* stage. In this stage the Helm Chart is deployed to the cluster 
3. This stage will fail because the Helm Chart does not satisfy the security requirements of the cluster
4. Read the error carefully, to find out the issue
5. Fix the error

> :bulb: Think about where and how you can modify *your Helm Chart*...

> :bulb: Background information: :link: [Customize Helm Chart before installing](https://helm.sh/docs/intro/using_helm/#customizing-the-chart-before-installing)

## :gear: Final attempt 

1. Run the pipeline again
2. The pipeline should now run successfully! :sunglasses:
3. Look at the scoreboard to see if your team has completed this lab! If your score is not updated, check the output of the *check_lab* stage and try again. Sometimes your app is not deployed immediately, so this check may fail the first time. Manually retry the check by clicking the *Retry* button :repeat: in the top right corner of the stage.
4. Continue with [lab05](/lab05)