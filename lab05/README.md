# Lab 05 - Web security

# :information_source: Summary

In the previous lab, you deployed your app to your AKS-nodepool with Helm. If you looked carefully, you might have spotted that there's also an admin page available at [`admin.teamX.azure.gluo.cloud`](https://admin.teamX.azure.gluo.cloud). In this lab, you will make sure nothing bad can happen to the app by accessing this admin page... To point you in the right direction, nikto, a web security scanner, will check some things for you.

# :hammer_and_wrench: Fix web security

## :gear: Attempt 1

1. Run the pipeline for lab 5 for the first time

## :mag: Error 1 - Dangerous HTTP-method

![lab5-pipeline-error](../images/lab05_pipeline_error.png) 

```console
- Nikto v2.1.6
---------------------------------------------------------------------------
+ Target IP:          XXX.XXX.XXX.XXX
+ Target Hostname:    admin.team1.azure.gluo.cloud
+ Target Port:        443
---------------------------------------------------------------------------
+ SSL Info:        Subject:  /O=Acme Co/CN=Kubernetes Ingress Controller Fake Certificate
                   Altnames: ingress.local
                   Ciphers:  TLS_AES_256_GCM_SHA384
                   Issuer:   /O=Acme Co/CN=Kubernetes Ingress Controller Fake Certificate
+ Start Time:         2023-05-08 10:52:06 (GMT0)
---------------------------------------------------------------------------
+ Server: No banner retrieved
+ No CGI Directories found (use '-C all' to force check all possible dirs)
+ Hostname 'admin.team1.azure.gluo.cloud' does not match certificate's names: Kubernetes, ingress.local
+ OSVDB-397: HTTP method 'PUT' allows clients to save files on the web server.
+ OSVDB-3092: /admin.html: This might be interesting.
+ 2518 requests: 0 error(s) and 5 item(s) reported on remote host
+ End Time:           2023-05-08 10:54:30 (GMT0) (144 seconds)
---------------------------------------------------------------------------
+ 1 host(s) tested
```

1. Analyse the error message in the output of the failed stage in the pipeline
2. The pipeline fails in the *upload* stage. In this stage the website is scanned with Nikto, a web server scanner
3. This stage will fail because the app is not secure and allows HTTP PUT-requests, which can be used to upload files to the server
4. Fix the error

> :bulb: It's still necessary that this PUT-method is usable from within the cluster, so don't disable it completely, only prevent a ***<u>route</u>*** being available from ***<u>outside</u>*** of the cluster. See if you can't [*change your Helm Chart*](/lab05/demo_app_values.yaml)

## :gear: Attempt 2

1. Run the pipeline again

## :mag: Error 2 - Insecure internal access

```console
$ kubectl apply -f lab05/networkPolicy.yaml
networkpolicy.networking.k8s.io/demo-app configured
$ echo "Testing port 8088 from a pod in namespace ${GROUP_NAME}.."
Testing port 8088 from a pod in namespace team1..
$ (timeout 30 kubectl run -i --rm --restart=Never --namespace ${GROUP_NAME} testpod1 --image=alpine -- sh -c "wget -qO- http://demo-app.${GROUP_NAME}:8088" && echo "Received a response, this is okay!" && exit 0 ) || ( kubectl delete pod -n ${GROUP_NAME} testpod1 && echo "Didn't receive a response, this should not happen!" && exit 1 )
If you don't see a command prompt, try pressing enter.
<!DOCTYPE html>
<html>
<head>
  <title>Gluo workshop - Admin</title>
  <link rel="icon" type="image/icon" href="/images/favicon.ico?">
</head>
... [Truncated for visibility] ...
</html>
pod "testpod1" deleted
Received a response, this is okay!
$ echo "Testing port 8088 from a pod in namespace testing-${GROUP_NAME}.."
Testing port 8088 from a pod in namespace testing-team1..
$ (timeout 30 kubectl run -i --rm --restart=Never --namespace testing-${GROUP_NAME} testpod2 --image=alpine -- sh -c "wget -qO- http://demo-app.${GROUP_NAME}:8088" && echo "Received a response, this should not happen!" && exit 1 ) || ( kubectl delete pod -n testing-${GROUP_NAME} testpod2 && echo "Didn't receive a response, this is okay!" && exit 0 )
If you don't see a command prompt, try pressing enter.
<!DOCTYPE html>
<html>
<head>
  <title>Gluo workshop - Admin</title>
  <link rel="icon" type="image/icon" href="/images/favicon.ico?">
</head>
... [Truncated for visibility] ...
</html>
pod "testpod2" deleted
Received a response, this should not happen!
```

1. Analyse the error message in the output of the failed stage in the pipeline
2. The pipeline fails in the *internal_traffic* stage. In this stage, access to the app's Pod on port 8088 is tested from within the cluster, but from another namespace `testing-teamX`
3. This stage will fail because access to the Pod on port 8088 is only allowed from within the namespace `teamX` of the app, but not from another namespace
4. Fix the error
5. As a start, look at the [`networkPolicy.yaml`](/lab05/networkPolicy.yaml) in this folder. This file contains a Network Policy that you can use to fix the error. Right now, it allows all namespaces to the Pod on ports `8080` and `8088`.

> :bulb: Background information: :link: [Kubernetes Network Policies](https://kubernetes.io/docs/concepts/services-networking/network-policies/)

> :bulb: The correct label for the name of a Namespace is `kubernetes.io/metadata.name`, not simply `name`

## :gear: Final attempt 

1. Run the pipeline again
2. The pipeline should now run successfully! :sunglasses:
3. Look at the scoreboard to see if your team has completed this lab! If your score is not updated, check the output of the *check_lab* stage and try again
4. Make a celebratory dance! :dancer: You've completed the DevSecOps workshop! :tada: Thank you for participating! :smiley: :wave: